﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="LatestItemsWithOptions.ascx.cs"
    Inherits="Modules_AspxLatestItemsWithOptions_LatestItemDisplay" %>
<div id="divLatestItemsWrapper" class="cssClassLatestItemWrapper">
    <div id="divLatestItemsHeader" class="cssClassItemHeader clearfix">
        <h2 id="latestItemHeader" class="sflocale">Items with Advanced Options
        </h2>
         <asp:Literal ID="ltrLIWORss" EnableViewState="false" runat="server" />
        <div id="ddlLatestSort" class="sort">
                <asp:Literal ID="ltrItemViewDetailSortBy" runat="server" EnableViewState="false" />
        </div>
    </div>
    <div id="divlatestItemsList" class="cssClassLatestItemsOption">
        <div class="itemList">
            <div id="divLatestItemList" class="cssClassLatestItemList">
                 <asp:Literal runat="server" id="litLatestItemList" />
            </div>
            <div class="cssClassPageNumber" id="divSearchPageNumber" style="display: none;">
                <div class="cssClassPageNumberMidBg">
                    <div id="liwoPagination">
                    </div>
                    <div class="cssClassViewPerPage">
                        <span class="sflocale">View Per Page:
                        </span>
                        <select id="ddlLatestPageSize" class="cssClassDropDown" autocomplete="off">
                            <asp:Literal ID="pageSizeOption" runat="server" EnableViewState="false" />
                        </select>
                    </div>
                </div>
                <div class="clear">
                </div>
            </div>
        </div>
    </div>
</div>
<input type="hidden" id="hdnPrice" />
<input type="hidden" id="hdnWeight" />
<input type="hidden" id="hdnQuantity" />
<script type="text/javascript">
    // <![CDATA[
    $(function () {
        $(this).LatestItemsWithOptionsLatestItemsListDetails({
            noOfLatestItems: '<%=NoOfLatestItems %>',
            allowAddToCart: '<%=AllowAddToCart%>',
            allowOutStockPurchase: '<%=AllowOutStockPurchase %>',
            rowToDisplay : '<%=rowTotalToDisplay %>',
            aspxLatestItemsWithOptionsModulePath: '<%=AspxLatestItemsWithOptionsModulePath %>',
            DefaultImagePath : '<%=DefaultImagePath %>'
        });
    });
    // ]]>
</script>
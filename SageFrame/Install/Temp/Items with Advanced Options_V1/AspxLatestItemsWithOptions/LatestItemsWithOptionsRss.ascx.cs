﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SageFrame.Web;
using AspxCommerce.Core;
using AspxCommerce.LatestItemsWithOptions;
using System.Collections;
using System.Xml;
using System.Text;

public partial class Modules_AspxCommerce_AspxLatestItemsWithOptions_LatestItemsWithOptionsRss : BaseAdministrationUserControl
{
    private static Hashtable hst = null;
    protected void Page_Load(object sender, EventArgs e)
    {
        int StoreID, PortalID;
        string UserName, CultureName;
        if (!IsPostBack)
        {
            GetPortalCommonInfo(out StoreID, out PortalID, out UserName, out CultureName);
            AspxCommonInfo aspxCommonObj = new AspxCommonInfo(StoreID, PortalID, UserName, CultureName);
            GetLatestItemOptionRssFeed(aspxCommonObj);
        }
        IncludeLanguageJS();
    }

    #region Latest Item With Options RSS Feed
    private void GetLatestItemOptionRssFeed(AspxCommonInfo aspxCommonObj)
    {
        try
        {
            AspxLatestItemWithOptionsController objLatestOpt = new AspxLatestItemWithOptionsController();
            List<LatestItemsOptionRssInfo> lstLatestItemRss = objLatestOpt.GetLatestOptionRssFeedContent(aspxCommonObj);
            BindLatestItemOptionRss(lstLatestItemRss, aspxCommonObj);
        }
        catch (Exception ex)
        {
            ProcessException(ex);
        }
    }
    #endregion

    #region Bind Latest Item With Option RSS Feed
    private void BindLatestItemOptionRss(List<LatestItemsOptionRssInfo> lstLatestItemRss, AspxCommonInfo aspxCommonObj)
    {
        string noImageUrl = string.Empty;
        StoreSettingConfig ssc = new StoreSettingConfig();
        noImageUrl = ssc.GetStoreSettingsByKey(StoreSetting.DefaultProductImageURL, aspxCommonObj.StoreID,
                                               aspxCommonObj.PortalID, aspxCommonObj.CultureName);
        string x = HttpContext.Current.Request.ApplicationPath;
        string authority = HttpContext.Current.Request.Url.Authority;
        string pageUrl = authority + x;
        string pageURL = Request.Url.AbsoluteUri;
        string[] path = pageURL.Split('?');
        string pagepath = path[0];
        HttpContext.Current.Response.Clear();
        HttpContext.Current.Response.ContentType = "text/xml";
        XmlTextWriter rssXml = new XmlTextWriter(HttpContext.Current.Response.OutputStream, Encoding.UTF8);

        rssXml.WriteStartDocument();
        rssXml.WriteStartElement("rss");
        rssXml.WriteAttributeString("version", "2.0");
        rssXml.WriteStartElement("channel");
        rssXml.WriteElementString("link", pagepath);
        rssXml.WriteElementString("title", getLocale("AspxCommerce Latest Items"));
        if (lstLatestItemRss.Count > 0)
        {
            foreach (LatestItemsOptionRssInfo rssItemData in lstLatestItemRss)
            {
                string imagePath = "Modules/AspxCommerce/AspxItemsManagement/uploads" + rssItemData.ImagePath;
                rssXml.WriteStartElement("item");
                rssXml.WriteElementString("title", rssItemData.ItemName);
                rssXml.WriteElementString("link",
                                          "http://" + pageUrl + "/item/" + rssItemData.SKU +
                                          SageFrameSettingKeys.PageExtension);
                rssXml.WriteStartElement("description");
                var description = "";
                if (rssItemData.ImagePath == "")
                {
                    imagePath = noImageUrl;
                }
                description = "<div><a href=http://" + pageUrl + "/item/" + rssItemData.SKU +
                              SageFrameSettingKeys.PageExtension + "><img src=http://" + pageUrl + "/" +
                              imagePath.Replace("uploads", "uploads/Small/") + " alt=" + rssItemData.ItemName + " /> </a></div>";
                description += "</br>" + HttpUtility.HtmlDecode(rssItemData.ShortDescription);
                rssXml.WriteCData(description);
                rssXml.WriteEndElement();
                rssXml.WriteElementString("pubDate", rssItemData.AddedOn);
                rssXml.WriteEndElement();
            }
        }
        else
        {
            rssXml.WriteStartElement("item");
            rssXml.WriteElementString("title", "");
            rssXml.WriteElementString("link", "");
            rssXml.WriteStartElement("description");
            var description = "";
            description = "<div><h2><span>" + getLocale("This store has no items listed yet!") + "</span></h2></div>";
            rssXml.WriteCData(description);
            rssXml.WriteEndElement();
            rssXml.WriteEndElement();
        }
        rssXml.WriteEndElement();
        rssXml.WriteEndElement();
        rssXml.WriteEndDocument();
        rssXml.Flush();
        rssXml.Close();
        HttpContext.Current.Response.End();
    }
    #endregion

    #region Localization
    private string getLocale(string messageKey)
    {
        string retStr = messageKey;
        if (hst != null && hst[messageKey] != null)
        {
            retStr = hst[messageKey].ToString();
        }
        return retStr;
    }
    #endregion
}
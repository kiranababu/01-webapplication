﻿using AspxCommerce.Core;
using SageFrame;
using SageFrame.Web;
using SanchiCommerce.Core;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Modules_UserReviewTab_UserReviewTab : BaseUserControl
{


    private int storeID,
               portalID,
               customerID;
    private string userName, cultureName;
    private string sessionCode = string.Empty;

    protected void Page_Load(object sender, EventArgs e)
    {
        string templateName = TemplateName;
        GetPortalCommonInfo(out storeID, out portalID, out customerID, out userName, out cultureName, out sessionCode);
        AspxCommonInfo aspxCommonObj = new AspxCommonInfo(storeID, portalID, userName, cultureName, customerID, sessionCode);

        if (!IsPostBack)
        {
            IncludeCss("UserReviewDetails", "/Templates/" + templateName + "/css/StarRating/jquery.rating.css",
                       "/Templates/" + templateName + "/css/JQueryUIFront/jquery-ui.all.css",
                       "/Templates/" + templateName + "/css/MessageBox/style.css",
                       "/Templates/" + templateName + "/css/FancyDropDown/fancy.css",
                       "/Templates/" + templateName + "/css/ToolTip/tooltip.css",
                       "/Templates/" + templateName + "/css/Scroll/scrollbars.css",
                       "/Templates/" + templateName + "/css/ResponsiveTab/responsive-tabs.css",
                       "/Modules/UserReviewTab/css/module.css"
                       );

            IncludeJs("UserReviewDetails", "/js/DateTime/date.js",
                      "/js/StarRating/jquery.rating.js",
                      "/js/ResponsiveTab/responsiveTabs.js",
                      "/js/PopUp/popbox.js", "/js/Scroll/mwheelIntent.js",
                      "/js/Scroll/jScrollPane.js",
                      "/js/VideoGallery/jquery.youtubepopup.min.js", "/js/jquery.labelify.js", "/js/encoder.js",
                      "/js/StarRating/jquery.rating.pack.js", "/js/StarRating/jquery.MetaData.js", "/js/Paging/jquery.pagination.js",
                      "/Modules/UserReviewTab/js/UserReviewTab.js","/Modules/UserReviewTab/Language/AspxUserReviewDetailTab.js");
        }
        IncludeLanguageJS();
        GetFormFieldList(aspxCommonObj);

    }
    private Hashtable hst = null;

    public void GetFormFieldList(AspxCommonInfo aspxCommonObj)
    {
        string resolvedUrl = ResolveUrl("~/");
        string modulePath = this.AppRelativeTemplateSourceDirectory;
        string aspxTemplateFolderPath = resolvedUrl + "Templates/" + TemplateName;
        string aspxRootPath = resolvedUrl;
        hst = AppLocalized.getLocale(modulePath);
        int RowTotal = 0;


        List<ReviewAndRatingByUser> lstRatingByUser = AspxRatingReviewController.GetRatingPerUser(1, 5, aspxCommonObj);

        List<ReviewAndRatingByUser> lstAvgRating = lstRatingByUser.GroupBy(x => x.UserReviewID).Select(g => g.First()).ToList<ReviewAndRatingByUser>();     // var lstAvgRating=lstRatingByUser.Distinct(a)

        StringBuilder ReviewTag = new StringBuilder();
      
        StringBuilder dynHtml = new StringBuilder();
        dynHtml.Append("<div id=\"dynUserReviewDetailsForm\" class=\"sfFormwrapper\">");
        dynHtml.Append("<div class=\"cssClassTabPanelTable1\">");
        dynHtml.Append("<div id=\"UserDetails_TabContainer\">");
        StringBuilder strUserRating = new StringBuilder();
        ReviewTag.Append("<div id=\"UserTab-Reviews\" style=\" height: 388px; margin-top:10px; height:100%; width:100%; \">");
        //ReviewTag.Append("<div><marquee  direction = \"up\" style=\" max-height: 130px;\">");
        
       ReviewTag.Append("<table cellspacing=\"0\" cellpadding=\"0\" border=\"0\" id=\"tblRatingPerUser\" >");
        if (lstRatingByUser != null && lstRatingByUser.Count > 0)
        {
            foreach (ReviewAndRatingByUser UserRating in lstAvgRating)
            {

                RowTotal = UserRating.RowTotal;
                strUserRating.Append("<tr><td><div class=\"cssClassRateReview\"><div class=\"cssClassUserRating\">");
                strUserRating.Append("<div class=\"cssClassuserRatingBox\">");
                StringBuilder ratingStars = new StringBuilder();
                string[] ratingTitle = { getLocale("Poor"), getLocale("OK"), getLocale("Fair"), getLocale("Good"), getLocale("Nice") };
                string[] ratingText = { "1", "2", "3", "4", "5" };
                int i = 0;
                string ratingTitleText = string.Empty;
                ratingStars.Append("<div class=\"cssClassRatingStar\"><div class=\"cssClassToolTip\">");

                for (i = 0; i < 5; i++)
                {
                    if (i < UserRating.Rating)
                    {
                        //ratingStars.Append("<span class=\"cssClassRatingTitle2 cssClassUserRatingTitle_");
                        //ratingStars.Append(UserRating.UserReviewID);
                        //ratingStars.Append("\">");
                        ////ratingStars.Append(ratingTitle[i]);
                        //ratingStars.Append("</span>");
                        ratingStars.Append("<span id=\"starDiv\" class=\"RatingStar filled\" >★");
                        //ratingStars.Append("<input name=\"avgRatePerUser");
                        //ratingStars.Append(UserRating.UserReviewID);
                        //ratingStars.Append("\"type=\"radio\" class=\"star-rate {split:1}\" disabled=\"disabled\" checked=\"checked\" value=\"");
                        // ratingStars.Append(ratingTitle[i]);
                        ratingStars.Append("</span>");
                       // ratingTitleText = ratingTitle[i];
                    }
                    else
                    {
                        ratingStars.Append("<span id=\"starDiv\" class=\"RatingStar emptystar\" >★");
                        //ratingStars.Append("<input name=\"avgRatePerUser");
                        //ratingStars.Append(UserRating.UserReviewID);
                        //ratingStars.Append("\" type=\"radio\" class=\"star-rate {split:1}\" disabled=\"disabled\" value=");
                        // ratingStars.Append(ratingTitle[i]);
                        ratingStars.Append("</span>");
                    }
                }
                ratingStars.Append("<input type=\"hidden\" value=\"");
                ratingStars.Append(ratingTitleText);
                ratingStars.Append("\" id=\"hdnRatingTitle");
                ratingStars.Append(UserRating.UserReviewID);
                ratingStars.Append("\"></input><span class=\"cssClassReviewId_");
                ratingStars.Append(UserRating.UserReviewID);
                ratingStars.Append("\">");
                ratingStars.Append("</span></div></div>");
                strUserRating.Append(ratingStars);
                strUserRating.Append("</div>");
                strUserRating.Append("<div class=\"cssClassRatingdesc\"><p class=\"cssClassRatingReviewDesc\">");
                strUserRating.Append(HttpUtility.HtmlDecode(UserRating.Review));
                strUserRating.Append("</p></div>");


                strUserRating.Append("<div class=\"cssClassRatingInfo\" style=\"float:left \"><p><span>");
                strUserRating.Append(getLocale("Reviewed by "));
                strUserRating.Append("<strong>");
                strUserRating.Append(UserRating.Username);
                strUserRating.Append("</strong></span>");
                strUserRating.Append("</div></div>");
             
                strUserRating.Append("</div></tr></td>");
                StringBuilder strScript = new StringBuilder();

                strScript.Append("$('input.star-rate').rating();");
                strScript.Append("$('#tblRatingPerUser tr:even').addClass('sfOdd');");
                strScript.Append("$('#tblRatingPerUser tr:odd').addClass('sfEven');");
                strUserRating.Append(GetScriptRun(strScript.ToString()));



            }
            ReviewTag.Append("<div id=\"PageID\">");
            string strPage = CreateDropdownPageSize(RowTotal);
            ReviewTag.Append(strPage);
            ReviewTag.Append("</div>");

        }
        else
        {
            strUserRating.Append(getLocale("Currently no reviews and ratings available"));
        }

        ReviewTag.Append(strUserRating.ToString());
        ReviewTag.Append("</table>");
        //ReviewTag.Append("</marquee></div>");
        ReviewTag.Append("</div>");
        dynHtml.Append(ReviewTag);
        dynHtml.Append("</div></div></div>");
        ltrDetailsForm2.Text = dynHtml.ToString();
    }



    public string CreateDropdownPageSize(int RowTotal)
    {

        StringBuilder strPage = new StringBuilder();
        strPage.Append("<div class=\"cssClassPageNumber\" id=\"divSearchPageNumber\">");
        strPage.Append("<div id=\"Pagination\">");
        strPage.Append("<div class=\"pagination\">");
        decimal noOfPages = ((decimal)RowTotal / 5);
        int numberOfPages = Convert.ToInt32(Math.Ceiling(noOfPages));
        for (int i = 1; i <= numberOfPages; i++)
        {
            if (i == 1)
            {
                strPage.Append("<span  class=\"current\">");
                strPage.Append(i);
                strPage.Append("</span>");
            }
            else
            {
                strPage.Append("<a href=\"javascript:void(0)\" onclick=\"UserDetailTab.GetRatingPerUser(");
                strPage.Append((((i - 1) * 5) + 1));
                strPage.Append(",");
                strPage.Append(5);
                strPage.Append(",");
                strPage.Append(i);
                strPage.Append(")\">");
                strPage.Append(i);
                strPage.Append("</a>");
            }

        }
        if (numberOfPages > 1)
        {
            strPage.Append("<a class=\"next\" href=\"javascript:void(0)\" onclick=\"UserDetailTab.GetRatingPerUser(");
            strPage.Append((((2 - 1) * 5) + 1));
            strPage.Append(",");
            strPage.Append(5);
            strPage.Append(",");
            strPage.Append(2);
            strPage.Append(")\">");
            strPage.Append("Next");
            strPage.Append("</a>");
        }
        int recordCount = 5;
        if (RowTotal < 5)
        {
            recordCount = RowTotal;
        }
        strPage.Append("<span class='showingPags'>Showing&nbsp;1-");
        strPage.Append(recordCount);
        strPage.Append("&nbsp;Of&nbsp;");
        strPage.Append(RowTotal);
        strPage.Append("&nbsp;records</span>");
        strPage.Append("</div>");
        strPage.Append("</div>");
        strPage.Append("<div class=\"cssClassViewPerPage\">");
        strPage.Append("<span>");
        strPage.Append(getLocale("View Per Page:"));
        strPage.Append("</span>");
        strPage.Append("<select class=\"sfListmenu\" id=\"ddlPageSize\">");
        strPage.Append("<option data-html-text='5' value='5'>");
        strPage.Append(5);
        strPage.Append("</option>");
        strPage.Append("<option data-html-text='10' value='10'>");
        strPage.Append(10);
        strPage.Append("</option>");
        strPage.Append("<option data-html-text='15' value='15'>");
        strPage.Append(15);
        strPage.Append("</option>");
        strPage.Append("<option data-html-text='20' value='20'>");
        strPage.Append(20);
        strPage.Append("</option>");
        strPage.Append("<option data-html-text='25' value='25'>");
        strPage.Append(25);
        strPage.Append("</option>");
        strPage.Append("<option data-html-text='40' value='40'>");
        strPage.Append(40);
        strPage.Append("</option>");
        strPage.Append("</select>");
        strPage.Append("</div>");
        strPage.Append("<div class=\"clear\">");
        strPage.Append("</div>");
        strPage.Append("</div>");
        return strPage.ToString();
    }


    private string GetScriptRun(string code)
    {
        StringBuilder sb = new StringBuilder();
        sb.Append("<script type=\"text/javascript\">$(document).ready(function(){");
        sb.Append(code);
        sb.Append("});</script>");
        return sb.ToString();
    }

    private string getLocale(string messageKey)
    {
        string retStr = messageKey;
        if (hst != null && hst[messageKey] != null)
        {
            retStr = hst[messageKey].ToString();
        }
        return retStr;
    }


}
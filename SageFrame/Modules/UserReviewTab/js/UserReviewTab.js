﻿var UserDetailTab = ''
$(function () {
    var arrUserDetailsReviewList = new Array();
    var arrUserReviewList = new Array();
    var IsExists = function (arr, val) {
        var isExist = false;
        for (var i = 0; i < arr.length; i++) {
            if (arr[i] == val) {
                isExist = true; break;
            }
        }
        return isExist;
    };
    var aspxCommonObj = function () {
        var aspxCommonInfo = {
            StoreID: SanchiCommerce.utils.GetStoreID(),
            PortalID: SanchiCommerce.utils.GetPortalID(),
            UserName: SanchiCommerce.utils.GetUserName(),
            CultureName: SanchiCommerce.utils.GetCultureName(),
            CustomerID: SanchiCommerce.utils.GetCustomerID(),
            SessionCode: SanchiCommerce.utils.GetSessionCode()
        };
        return aspxCommonInfo;
    };
    UserDetailTab = {

        config: {
            isPostBack: false,
            async: true,
            cache: true,
            type: 'POST',
            contentType: "application/json; charset=utf-8",
            data: '{}',
            dataType: 'json',
            baseURL: SanchiCommerce.utils.GetAspxServicePath(),
            method: "",
            url: "",
            oncomplete: 0,
            ajaxCallMode: "",
            error: ""
        },
        ajaxCall: function (config) {
            $.ajax({
                type: UserDetailTab.config.type,
                contentType: UserDetailTab.config.contentType,
                cache: UserDetailTab.config.cache,
                async: UserDetailTab.config.async,
                url: UserDetailTab.config.url,
                data: UserDetailTab.config.data,
                dataType: UserDetailTab.config.dataType,
                success: UserDetailTab.config.ajaxCallMode,
                error: UserDetailTab.config.error,
                complete: UserDetailTab.oncomplete
            });
        },
        BinduserRatingByUser: function (Reviews, index) {
            debugger;
            arrUserDetailsReviewList.push(Reviews);
            if (!IsExists(UserReview, Reviews.UserReviewID)) {
                UserReview.push(Reviews.UserReviewID);
                arrUserReviewList.push(Reviews);
            }

        },
        BindStarRatingAveragePerUser: function (UserReviewID, Rating) {
            var ratingStars = '';
            var ratingTitle = [getLocale(AspxUserReviewDetailTab, "Poor"), getLocale(AspxUserReviewDetailTab, "Ok"), getLocale(AspxUserReviewDetailTab, "Fair"), getLocale(AspxUserReviewDetailTab, "Good"), getLocale(AspxUserReviewDetailTab, "Nice")]; var ratingText = ["1", "2", "3", "4", "5"];
            var i = 0;
            var ratingTitleText = '';
            ratingStars += '<div class="cssClassRatingStar"><div class="cssClassToolTip">';
    
            for (i = 0; i < 5; i++) {
                if (i < Rating) {
                    ratingStars += '<span id="starDiv" class="RatingStar filled">★</span>';
                    //ratingStars += '<input name="avgRatePerUser' + UserReviewID + '" type="radio" class="star-rate {split:1}" disabled="disabled" checked="checked" value="' + ratingTitle[i] + '" />';
                    //   ratingTitleText = ratingTitle[i];
                } else {
                    ratingStars += '<span id="starDiv" class="RatingStar emptystar">★</span>';
                    //ratingStars += '<input name="avgRatePerUser' + UserReviewID + '" type="radio" class="star-rate {split:1}" disabled="disabled" value="' + ratingTitle[i] + '" />';
                }
            }
            ratingStars += '<span class="cssClassRatingTitle2 cssClassUserRatingTitle_' + UserReviewID + '"></span>';
            ratingStars += '<input type="hidden" value="' + ratingTitleText + '" id="hdnRatingTitle' + UserReviewID + '"></input><span class=" cssClassReviewId_' + UserReviewID + '"></span></div></div><div class="cssClassClear"></div>';
            return ratingStars;
        },
        BindAverageUserRating: function (Review) {
            var userRatings = '';
            userRatings += '<tr><td><div class="cssClassRateReview"><div class="cssClassUserRating">';
            userRatings += '<div class="cssClassuserRatingBox">' + UserDetailTab.BindStarRatingAveragePerUser(Review.UserReviewID, Review.Rating) + '</div>';
            userRatings += '<div class="cssClassRatingdesc"><p class="cssClassRatingReviewDesc">' + Encoder.htmlDecode(Review.Review) + '</p></div>';

            userRatings += '<div class="cssClassRatingInfo"><p><span>' + getLocale(AspxUserReviewDetailTab, 'Reviewed by') + ' <strong>' + Review.Username + '</strong></span></p></div></div>';

        

            userRatings += '</div></td></tr>';
            $("#tblRatingPerUser").append(userRatings);
            var ratingToolTip = $("#hdnRatingTitle" + Review.UserReviewID + "").val();
            $(".cssClassUserRatingTitle_" + Review.UserReviewID + "").html(ratingToolTip);
        },
        BindPerUserIndividualRatings: function (UserReviewID, rating) {
            var userRatingStarsDetailsInfo = '';
            var ratingTitle = [getLocale(AspxUserReviewDetailTab, "Poor"), getLocale(AspxUserReviewDetailTab, "Ok"), getLocale(AspxUserReviewDetailTab, "Fair"), getLocale(AspxUserReviewDetailTab, "Good"), getLocale(AspxUserReviewDetailTab, "Nice")]; var ratingText = ["1", "2", "3", "4", "5"];
            var i = 0;
            userRatingStarsDetailsInfo += '<div class="">';
            for (i = 0; i < 5; i++) {
                if (i < Rating) {
                //userRatingStarsDetailsInfo += '<input name="avgUserDetailRate' + UserReviewID + '" type="radio" class="star-rate {split:1}" disabled="disabled" checked="checked" value="' + ratingTitle[i] + '" />';
                    userRatingStarsDetailsInfo += '<span id="starDiv" class="RatingStar filled " >★</span>';
            } else {
                    userRatingStarsDetailsInfo += '<span id="starDiv" class="RatingStar emptystar">★</span>';
            // userRatingStarsDetailsInfo += '<input name="avgUserDetailRate' + UserReviewID + '" type="radio" class="star-rate {split:1}" disabled="disabled" value="' + ratingTitle[i] + '" />';
        }
            }
            userRatingStarsDetailsInfo += '</div>';
            $('#tblRatingPerUser span.cssClassReviewId_' + UserReviewID + '').append(userRatingStarsDetailsInfo);
        },
        pageselectCallback: function (page_index, jq, execute) {
            if (execute) {
                var User_per_page = $('#ddlPageSize').val();
                var max_elem = Math.min((page_index + 1) * User_per_page, arrUserReviewList.length);
                $("#tblRatingPerUser").html('');
                UserReview = [];
                for (var i = 0; i < max_elem; i++) {
                    UserDetailTab.BindAverageUserRating(arrUserReviewList[i]);
                    UserReview.push(arrUserReviewList[i].UserReviewID);
                }
                //$.each(arrUserDetailsReviewList, function (index, Review) {
                //    if (IsExists(UserReview, Review.UserReviewID)) {
                //       // UserDetailTab.BindPerUserIndividualRatings(Review.UserReviewID, Review.Rating);
                //    }
                //});
                $('input.star-rate').rating();
                $("#tblRatingPerUser tr:even").addClass("sfOdd");
                $("#tblRatingPerUser tr:odd").addClass("sfEven");
            }
            return false;
        },
        getOptionsFromForm: function () {
            var opt = { callback: UserDetailTab.pageselectCallback };
            opt.items_per_page = $('#ddlPageSize').val();
            opt.current_page = currentpage;
            opt.callfunction = true,
                 opt.function_name = { name: UserDetailTab.GetRatingPerUser, limit: $('#ddlPageSize').val() },
                opt.prev_text = "Prev";
            opt.next_text = "Next";
            opt.prev_show_always = false;
            opt.next_show_always = false;
            return opt;
        },
        BindUserRatingPerUser: function (msg) {
            arrUserDetailsReviewList.length = 0;
            arrUserReviewList.length = 0;
            var rowTotal = 0;
            var length = msg.d.length;
            if (length > 0) {
                var Reviews;
                for (var index = 0; index < length; index++) {
                    Reviews = msg.d[index];
                    UserDetailTab.BinduserRatingByUser(Reviews, index);
                    rowTotal = Reviews.RowTotal;
                };
                var optInit = UserDetailTab.getOptionsFromForm();
                $("#Pagination").pagination(rowTotal, optInit);
               $("#divSearchPageNumber").show();
            } else {
                $("#divSearchPageNumber").hide();
                var avgRating = "<tr><td>" + getLocale(AspxUserReviewDetailTab, "Currently no rating and reviews are available.") + "</td></tr>";
                $("#tblRatingPerUser").append(avgRating);
            }
        },
        GetRatingPerUser: function (offset, limit, currenpage) {
            UserReview = [];
            currentpage = currenpage;
            var param = JSON2.stringify({ offset: offset, limit: limit, aspxCommonObj: aspxCommonObj() });
            UserDetailTab.config.method = "AspxCommonHandler.ashx/GetRatingPerUser";
            UserDetailTab.config.url = UserDetailTab.config.baseURL + UserDetailTab.config.method;
            UserDetailTab.config.data = param;
            UserDetailTab.config.ajaxCallMode = UserDetailTab.BindUserRatingPerUser;
           UserDetailTab.ajaxCall(UserDetailTab.config);
        },
        noSpaceonTag: function () {
            $(".classTag").on("keyup", function (e) {
                if (e.keyCode == "32") {
                    e.preventDefault();
                }
            });
            $(".classTag").on("keydown", function (e) {
                if (e.keyCode == "32") {
                    e.preventDefault();
                }
            });
            $('.classTag').on("input", function () {
                $(this).val($(this).val().replace(/ /g, ""));
            });
        },
        Init: function () {
            UserDetailTab.noSpaceonTag();
            $("#dynUserReviewDetailsForm").show();
            $("#dynUserReviewDetailsForm").find("#ddlPageSize").bind("change", function () {
                var items_per_page = $(this).val();
                var offset = 1;
                UserDetailTab.GetRatingPerUser(offset, items_per_page, 0);
            });
            $(".cssClassTotalReviews").bind("click", function () {
                $.metadata.setType("class");
                var $tabs = $('#UserDetails_TabContainer').tabs();
                $("#UserDetails_TabContainer").find('ul').removeClass();
                $("#UserDetails_TabContainer ").find("ul").addClass("responsive-tabs__list");
                $("#UserDetails_TabContainer").removeClass();
                $("#UserDetails_TabContainer").addClass("responsive-tabs responsive-tabs--enabled");
                $("#tablist1-tab4").trigger("click");
            });
        }
    };
    UserDetailTab.Init();
});
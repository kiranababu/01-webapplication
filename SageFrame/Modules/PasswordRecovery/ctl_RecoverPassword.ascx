﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ctl_RecoverPassword.ascx.cs"
    Inherits="SageFrame.Modules.PasswordRecovery.ctl_RecoverPassword" %>

<div class="sfRecoverPasswordPage" runat="server" id="divRecoverpwd">
  <div class="sfFormwrapper sfLogininside" >
    <asp:Wizard ID="wzdPasswordRecover" runat="server" DisplaySideBar="False" ActiveStepIndex="0"
        DisplayCancelButton="True" OnCancelButtonClick="CancelButton_Click" OnNextButtonClick="wzdPasswordRecover_NextButtonClick"
        OnFinishButtonClick="wzdPasswordRecover_FinishButtonClick" Width="100%" 
          meta:resourcekey="wzdPasswordRecoverResource1">
      <FinishNavigationTemplate>
        <div class="sfButtonwrapper" >
          <asp:Button ID="FinishButton" runat="server" AlternateText="Finish"  
                CommandName="MoveComplete" CssClass="sfBtn"
                    Text="Finish" meta:resourcekey="FinishButtonResource1" />
        </div>
      </FinishNavigationTemplate>
      <StartNavigationTemplate>
        <div class="sfButtonwrapper" style="margin: 1px 0 0 0!important;">
          <asp:Button ID="StartNextButton" runat="server" AlternateText="Submit"  CommandName="MoveNext" CssClass="sfBtn" ValidationGroup="vdgRecoveredPassword" Text="Submit" meta:resourcekey="StartNextButtonResource1" />
          <asp:Button ID="CancelButton" runat="server" style="display:none;" AlternateText="Cancel" CommandName="Cancel" CssClass="sfBtn" Text="Cancel" meta:resourcekey="CancelButtonResource1" />
        </div>
      </StartNavigationTemplate>
      <StepNavigationTemplate>
        <div class="sfButtonwrapper">
          <asp:Button ID="StepNextButton" runat="server" AlternateText="Next"  
                CommandName="MoveNext" CssClass="sfBtn" ValidationGroup="vdgRecoveredPassword"
                    Text="Next" meta:resourcekey="StepNextButtonResource1" />
        </div>
      </StepNavigationTemplate>
      <WizardSteps>
        <asp:WizardStep ID="WizardStep1" runat="server" Title="Setting New Password" 
              meta:resourcekey="WizardStep1Resource1"> <%= helpTemplate %>
          <p class="sfFormwrapper">
               
               <p class="sfPassword ">  <asp:HiddenField ID="hdnRecoveryCode" runat="server" />
                  <asp:TextBox ID="txtPassword" runat="server" TextMode="Password" placeholder="New Password"
                        CssClass="sfInputbox password" MaxLength="20" meta:resourcekey="txtPasswordResource1"></asp:TextBox>
                  <asp:RequiredFieldValidator ID="rfvRecoveredPassword" runat="server" ControlToValidate="txtPassword"
                                    ValidationGroup="vdgRecoveredPassword" ErrorMessage="Please enter a password." 
                        CssClass="sfError" meta:resourcekey="rfvRecoveredPasswordResource1"></asp:RequiredFieldValidator> </p>
               
                   <p class="sfPassword "> <asp:TextBox ID="txtRetypePassword" runat="server" TextMode="Password"  placeholder="Retype Password"
                        CssClass="sfInputbox" MaxLength="20" meta:resourcekey="txtRetypePasswordResource1"></asp:TextBox>
                  <%--<asp:RequiredFieldValidator ID="rfvRetypePassword"  runat="server" ControlToValidate="txtRetypePassword"
                                    ValidationGroup="vdgRecoveredPassword" ErrorMessage="Please retype the password." 
                        CssClass="sfError" meta:resourcekey="rfvRetypePasswordResource1"></asp:RequiredFieldValidator>--%>
                <asp:CompareValidator ID="cvPassword" runat="server" ErrorMessage="*" CssClass="sfError"
                                    ControlToCompare="txtPassword" 
                        ControlToValidate="txtRetypePassword"  ValidationGroup="vdgRecoveredPassword" 
                        meta:resourcekey="cvPasswordResource1" style="width:100%;"></asp:CompareValidator></p>
             
                <div class="sfError" style="display:none;"><asp:ValidationSummary ID="ValidationSummary1" runat="server" 
                        ValidationGroup="vdgRecoveredPassword" 
                        meta:resourcekey="ValidationSummary1Resource1" /></div>
             <p style="color: #2D4C80;">
                    <span class="cssClassForgotPass">
                        <asp:Label ID="Label1" runat="server" Text="Minimum number of characters is 6."></asp:Label>
                      
                    </span>
                </p>
              </asp:WizardStep>
  

        
      

        <asp:WizardStep ID="WizardStep2" runat="server" Title="Finished Template" 
              meta:resourcekey="WizardStep2Resource1">
          <asp:Literal ID="litPasswordChangedSuccessful" runat="server" 
                meta:resourcekey="litPasswordChangedSuccessfulResource1"></asp:Literal>
        </asp:WizardStep>
      </WizardSteps>
    </asp:Wizard>
  </div>
</div>
<script type="text/javascript">
    //<![CDATA[
    $(document).ready(function () {
    
        $(".sfLocalee").SystemLocalize();
     
        var pwdID = '#' + '<%=txtPassword.ClientID%>';
        var cpwdID = '#' + '<%=txtRetypePassword.ClientID%>';
        $('#minchar').remove();
        $(pwdID).val('');
   
        $(pwdID).on("change", function () {
            var len = $(this).val().length;
            if (len < 6 && len != 0) {
                $(this).after('<label class="sfError" id="lblPassswordLength"><br/>Password must be at least 6 chars long.</label>');
                return false;
              
            }
            else {
                $('#lblPassswordLength').remove();
            }
        });
        $(pwdID).click(function () {
            $('#lblPassswordLength').remove();
        });
       

      //  $('.password').pstrength({ minchar: 6 }); 

    });
    function pageLoad(sender, args) {
        if (args.get_isPartialLoad()) {
       
          //  $('.password').pstrength({ minchar: 6 });
        }
    }


    //]]>	
</script>

<script type="text/javascript">

    $(document).ready(function () {
        validate();
        $('input').on('keyup', validate);
    });

    var Startbtn = document.getElementById('<%=wzdPasswordRecover.ClientID %>');
    var id = Startbtn.childNodes[1].childNodes[1].childNodes[1].childNodes[1].childNodes[1].id;
    function validate() {
       
        var pswd = $("#<%= txtPassword.ClientID %>").val();
        var Repswd = $("#<%= txtRetypePassword.ClientID %>").val();
        if (pswd == "" || Repswd == "") {
            $("input[type=submit]").prop("disabled", true);
            $(".sfBtn").addClass("notallowed");

        } else {
            $("input[type=submit]").prop("disabled", false);
            $(".sfBtn").removeClass("notallowed");
        }
        return;
    }

    </script>
<style type="text/css"> 
    .notallowed{
        cursor:not-allowed!important;
    }
</style>
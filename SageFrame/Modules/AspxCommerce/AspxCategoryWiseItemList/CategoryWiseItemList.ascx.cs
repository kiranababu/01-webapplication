﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AspxCommerce.Core;
using SageFrame.Framework;
using SageFrame.Web;
using System.Web;
using AspxCommerce.CategoryWiseItemList;

public partial class Modules_AspxCommerce_AspxCategoryWiseItemsList_CategoryWiseItemList : BaseAdministrationUserControl
{
    public string UserIp;
    public string CountryName = string.Empty;
    public string SessionCode = string.Empty;
    public int StoreID, PortalID, CustomerID;
    public int NoOfItemsInCategory, RowTotal, NoOfItemsDisplayInARow;
    public string UserName, CultureName, CatWiseItemModulePath;
    public string DefaultImagePath, AllowAddToCart, AllowOutStockPurchase;
    public int Nomargin = 0;
    public int Count;

    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {

            GetPortalCommonInfo(out StoreID, out PortalID, out UserName, out CultureName);
            if (!IsPostBack)
            {
                IncludeCss("categoryWiseItemList", "/Templates/" + TemplateName + "/css/MessageBox/style.css", "/Templates/" + TemplateName + "/css/ToolTip/tooltip.css", "/Modules/AspxCommerce/AspxCategoryWiseItemList/css/module.css");
                IncludeJs("categoryWiseItemList", "/js/Paging/jquery.pagination.js", "/js/jquery.tipsy.js", "/js/encoder.js");
                if (HttpContext.Current.Session.SessionID != null)
                {
                    SessionCode = HttpContext.Current.Session.SessionID.ToString();
                }
                CatWiseItemModulePath = ResolveUrl(this.AppRelativeTemplateSourceDirectory);
                UserIp = HttpContext.Current.Request.UserHostAddress;
                IPAddressToCountryResolver ipToCountry = new IPAddressToCountryResolver();
                ipToCountry.GetCountry(UserIp, out CountryName);

                StoreSettingConfig ssc = new StoreSettingConfig();
                DefaultImagePath = ssc.GetStoreSettingsByKey(StoreSetting.DefaultProductImageURL, StoreID, PortalID,
                                                             CultureName);
                AllowAddToCart = ssc.GetStoreSettingsByKey(StoreSetting.ShowAddToCartButton, StoreID, PortalID, CultureName);
                AllowOutStockPurchase = ssc.GetStoreSettingsByKey(StoreSetting.AllowOutStockPurchase, StoreID, PortalID,
                                                                  CultureName);
                NoOfItemsDisplayInARow = int.Parse(ssc.GetStoreSettingsByKey(StoreSetting.NoOfDisplayItems, StoreID, PortalID, CultureName));

            }
            IncludeLanguageJS();
            GetCategoryWiseItemSetting();
        }
        catch (Exception ex)
        {
            ProcessException(ex);
        }
    }

    Hashtable hst = null;
    private void GetCategoryWiseItemSetting()
    {
        GetCategoryWiseItemList();

    }
    StringBuilder CatWiseItemContains = new StringBuilder();
    private void GetCategoryWiseItemList()
    {
        AspxCommonInfo aspxCommonObj = new AspxCommonInfo();
        aspxCommonObj.StoreID = StoreID;
        aspxCommonObj.PortalID = PortalID;
        aspxCommonObj.UserName = UserName;
        aspxCommonObj.CultureName = CultureName;
        int offset = 1;
        int limit = 5;
        string pageExtension = SageFrameSettingKeys.PageExtension;
        AspxCatWiseItemController objCatWise = new AspxCatWiseItemController();
        var result =
             objCatWise.GetCategoryWiseItemListWithSetting(offset, limit, NoOfItemsInCategory, aspxCommonObj);
        List<CategoryWiseItemInfo> catWiseItemsInfo = (List<CategoryWiseItemInfo>)result[0];
        catWiseItemsInfo = catWiseItemsInfo.OrderByDescending(x => x.CategoryID).ToList();
        var setting = (CategoryWiseitemSettings)result[1];
        NoOfItemsInCategory = setting.NumberOfItemsInCategory;
        if (catWiseItemsInfo != null && catWiseItemsInfo.Count > 0)
        {
            List<int> catIDs = new List<int>();
            StringBuilder html = new StringBuilder();
            foreach (CategoryWiseItemInfo item in catWiseItemsInfo)
            {
                if (IsExistedCategory(catIDs, item.CategoryID))
                {
                    if (item.ItemRowNum <= NoOfItemsInCategory + 1)
                    {
                        Count = Count + 1;
                        if ((Count + 1) % NoOfItemsInCategory == 0)
                        {
                            Nomargin = 1;
                        }
                        else
                        {
                            Nomargin = 0;
                        }
                        BindCategoryItems(item);
                        html.Append(CatWiseItemContains);
                    }
                }
                else
                {
                    Count = 0;
                    Nomargin = 0;

                    if (catWiseItemsInfo.IndexOf(item) > 0)
                    {
                        html.Append("</div></div>");
                    }
                    catIDs.Add(item.CategoryID);
                    html.Append("<div class=\"cssCategoryBlock\">");
                    html.Append("<label class=\"classCategoryName\" id=\"classCategoryName_");
                    html.Append(item.CategoryID);
                    html.Append("\"><h2 class=\"cssClassMiddleHeader\"><span>");
                    html.Append(item.CategoryName);
                    html.Append("</span></h2></label>");
                    html.Append("<div id=\"divViewMore_" + item.CategoryID + "\">");
                    html.Append("</div>");
                    html.Append("<div class=\"categorywrap clearfix\" id=div_");
                    html.Append(item.CategoryID);
                    html.Append("'>");
                    BindCategoryItems(item);
                    html.Append(CatWiseItemContains);
                }
            }
            html.Append("</div></div>");
            ltrCategoryWiseItem.Text = html.ToString();
        }
        else
        {
            StringBuilder html = new StringBuilder();
            html.Append("<span class=\"cssClassNotFound sflocale\">" + getLocale("This store has no items listed yet!") +
                        "</span>");
            ltrCategoryWiseItem.Text = html.ToString();
        }
    }

    private void BindCategoryItems(CategoryWiseItemInfo item)
    {
        string pageExtension = SageFrameSettingKeys.PageExtension;
        string aspxTemplateFolderPath = ResolveUrl("~/") + "Templates/" + TemplateName;
        CatWiseItemContains = new StringBuilder();
        string aspxRootPath = ResolveUrl("~/");
        RowTotal = item.RowTotal;
        string imagePath = "Modules/AspxCommerce/AspxItemsManagement/uploads/" + item.ImagePath;
        if (item.ImagePath == "")
        {
            imagePath = DefaultImagePath;
        }
        if (item.AlternateText == "")
        {
            item.AlternateText = item.Name;
        }
        string itemPrice = Math.Round(double.Parse((item.Price).ToString()), 2).ToString();
        string itemPriceRate = Math.Round(double.Parse((item.Price).ToString()), 2).ToString("N2");
        string name = string.Empty;
        if (item.Name.Length > 50)
        {
            name = item.Name.Substring(0, 50);
            int index = 0;
            index = name.LastIndexOf(' ');
            name = name.Substring(0, index);
            name = name + "...";
        }
        else
        {
            name = item.Name;
        }
        if (item.ItemRowNum <= NoOfItemsInCategory)
        {
            CatWiseItemContains.Append("<div class=\"classItemsList_" + item.CategoryID + "\">");
            if (Nomargin == 1)
            {
                CatWiseItemContains.Append("<div class=\"cssLatestItemInfo cssClassProductsBox cssClassProductsBoxNoMargin\">");
            }
            else
            {
                CatWiseItemContains.Append("<div class=\"cssLatestItemInfo cssClassProductsBox\">");
            }

            var hrefItem = aspxRedirectPath + "item/" + AspxUtility.fixedEncodeURIComponent(item.SKU) + pageExtension;
            CatWiseItemContains.Append("<div id=\"productImageWrapID_");
            CatWiseItemContains.Append(item.ItemID);
            CatWiseItemContains.Append("\" class=\"cssClassProductsBoxInfo\" costvariantItem=");
            CatWiseItemContains.Append(item.IsCostVariantItem);
            CatWiseItemContains.Append("  itemid=\"");
            CatWiseItemContains.Append(item.ItemID);
            CatWiseItemContains.Append("\"><h3>");
            CatWiseItemContains.Append(item.SKU);
            CatWiseItemContains.Append("</h3>");
            CatWiseItemContains.Append("<div id=\"divitemImage\" class=\"cssClassProductPicture\"><a href=\"");
            CatWiseItemContains.Append(hrefItem);
            CatWiseItemContains.Append("\" ><img id=\"");
            CatWiseItemContains.Append(item.ItemID);
            CatWiseItemContains.Append("\"  alt=\"");
            CatWiseItemContains.Append(item.AlternateText);
            CatWiseItemContains.Append("\"  title=\"");
            CatWiseItemContains.Append(item.AlternateText);
            CatWiseItemContains.Append("\" data-original=\"");
            CatWiseItemContains.Append(aspxTemplateFolderPath);
            CatWiseItemContains.Append("/images/loader_100x12.gif\" src=\"");
            CatWiseItemContains.Append(aspxRootPath);
            CatWiseItemContains.Append(imagePath.Replace("uploads", "uploads/Medium"));
            CatWiseItemContains.Append("\"/></a></div>");
            CatWiseItemContains.Append("<div class='productName'><a href=\"" + hrefItem + "\" title=\"" + item.Name + "\"><h2>");
            CatWiseItemContains.Append(name);
            CatWiseItemContains.Append("</h2></a></div>");
            if (item.HidePrice != true)
            {
                if (item.ListPrice != null)
                {
                    string listPrice = Math.Round(double.Parse(item.ListPrice.ToString()), 2).ToString();
                    string strAmount = Math.Round((double)(item.ListPrice), 2).ToString("N2");
                    CatWiseItemContains.Append("<div class=\"cssClassProductPriceBox\"><div class=\"cssClassProductPrice\">");
                    CatWiseItemContains.Append("<p class=\"cssClassProductOffPrice\">");
                    CatWiseItemContains.Append("<span class=\"cssClassFormatCurrency\" value=");
                    CatWiseItemContains.Append(listPrice);
                    CatWiseItemContains.Append(">");
                    CatWiseItemContains.Append(strAmount);
                    CatWiseItemContains.Append("<p class=\"cssClassProductRealPrice \" >");
                    CatWiseItemContains.Append("<span class=\"cssClassFormatCurrency\" value=");
                    CatWiseItemContains.Append(itemPrice);
                    CatWiseItemContains.Append(">");
                    CatWiseItemContains.Append(itemPriceRate);
                    CatWiseItemContains.Append("</span></p></div></div>");
                }
                else
                {
                    CatWiseItemContains.Append("<div class=\"cssClassProductPriceBox\"><div class=\"cssClassProductPrice\">");
                    CatWiseItemContains.Append("<p class=\"cssClassProductRealPrice \" >");
                    CatWiseItemContains.Append("<span class=\"cssClassFormatCurrency\" value=");
                    CatWiseItemContains.Append(itemPrice);
                    CatWiseItemContains.Append(">");
                    CatWiseItemContains.Append(itemPriceRate);
                    CatWiseItemContains.Append("</span></p></div></div>");
                }
            }
            CatWiseItemContains.Append("<div class=\"cssClassProductDetail\"><p><a href=\"");
            CatWiseItemContains.Append(aspxRedirectPath);
            CatWiseItemContains.Append("item/");
            CatWiseItemContains.Append(item.SKU);
            CatWiseItemContains.Append(pageExtension);
            CatWiseItemContains.Append("\">");
            CatWiseItemContains.Append(getLocale("Details"));
            CatWiseItemContains.Append("</a></p></div>");
            CatWiseItemContains.Append("</div><br>");
            CatWiseItemContains.Append("<div class=\"sfButtonwrapper\">");
            CatWiseItemContains.Append("<div class=\"cssClassCompareButton\">");
            CatWiseItemContains.Append("<input type=\"hidden\" name='itemcompare' value=");
            CatWiseItemContains.Append(item.ItemID);
            CatWiseItemContains.Append(",'");
            CatWiseItemContains.Append(item.SKU);
            CatWiseItemContains.Append("',this  />");
            CatWiseItemContains.Append(" </div>");
            CatWiseItemContains.Append("</div>");

            string itemSKU = item.SKU;
            string itemName = item.Name;
            string itemPriceValue = item.Price.ToString();
            StringBuilder dataContent = new StringBuilder();
            dataContent.Append("data-class=\"addtoCart\" data-type=\"button\" data-addtocart=\"");
            dataContent.Append("addtocart");
            dataContent.Append(item.ItemID);
            dataContent.Append("\" data-title=\"");
            dataContent.Append(itemName);
            dataContent.Append("\" data-onclick=\"AspxCommerce.RootFunction.AddToCartFromJS(");
            dataContent.Append(item.ItemID);
            dataContent.Append(",");
            dataContent.Append(itemPriceValue);
            dataContent.Append(",'");
            dataContent.Append(itemSKU);
            dataContent.Append("',");
            dataContent.Append(1);
            dataContent.Append(",'");
            dataContent.Append(item.IsCostVariantItem);
            dataContent.Append("',this);\"");

            CatWiseItemContains.Append("<div class=\"clearfix\">");
            if (AllowAddToCart.ToLower() == "true")
            {
                if (AllowOutStockPurchase.ToLower() == "false")
                {
                    if (bool.Parse(item.IsOutOfStock.ToString()) == true)
                    {

                        CatWiseItemContains.Append("<div class=\"cssClassAddtoCard\"><div class=\"sfButtonwrapper cssClassOutOfStock\"  data-ItemTypeID=\"");
                        CatWiseItemContains.Append(item.ItemTypeID);
                        CatWiseItemContains.Append("\" data-ItemID=\"");
                        CatWiseItemContains.Append(item.ItemID);
                        CatWiseItemContains.Append("\" ");
                        CatWiseItemContains.Append(dataContent);
                        CatWiseItemContains.Append(">");
                        CatWiseItemContains.Append("<button type=\"button\"><span>");
                        CatWiseItemContains.Append(getLocale("Out Of Stock"));
                        CatWiseItemContains.Append("</span></button></div></div>");
                    }
                    else
                    {
                        CatWiseItemContains.Append("<div class=\"cssClassAddtoCard\"><div data-ItemTypeID=\"");
                        CatWiseItemContains.Append(item.ItemTypeID);
                        CatWiseItemContains.Append("\" data-ItemID=\"");
                        CatWiseItemContains.Append(item.ItemID);
                        CatWiseItemContains.Append("\" ");
                        CatWiseItemContains.Append(dataContent);
                        CatWiseItemContains.Append(" class=\"sfButtonwrapper\">");
                        CatWiseItemContains.Append("<label class='i-cart cssClassCartLabel cssClassGreenBtn'><button type=\"button\" class=\"addtoCart\"");
                        CatWiseItemContains.Append("addtocart=\"");
                        CatWiseItemContains.Append("addtocart");
                        CatWiseItemContains.Append(item.ItemID);
                        CatWiseItemContains.Append("\" title=\"");
                        CatWiseItemContains.Append(itemName);
                        CatWiseItemContains.Append("\" onclick=\"AspxCommerce.RootFunction.AddToCartFromJS(");
                        CatWiseItemContains.Append(item.ItemID);
                        CatWiseItemContains.Append(",");
                        CatWiseItemContains.Append(itemPriceValue);
                        CatWiseItemContains.Append(",'");
                        CatWiseItemContains.Append(itemSKU);
                        CatWiseItemContains.Append("',");
                        CatWiseItemContains.Append(1);
                        CatWiseItemContains.Append(",'");
                        CatWiseItemContains.Append(item.IsCostVariantItem);
                        CatWiseItemContains.Append("',this);\">");
                        CatWiseItemContains.Append(getLocale("Cart +"));
                        CatWiseItemContains.Append("</button></label></div></div>");
                    }
                }
                else
                {
                    CatWiseItemContains.Append("<div class=\"cssClassAddtoCard\"><div data-ItemTypeID=\"");
                    CatWiseItemContains.Append(item.ItemTypeID);
                    CatWiseItemContains.Append("\" data-ItemID=\"");
                    CatWiseItemContains.Append(item.ItemID);
                    CatWiseItemContains.Append("\"");
                    CatWiseItemContains.Append(dataContent);
                    CatWiseItemContains.Append(" class=\"sfButtonwrapper\">");
                    CatWiseItemContains.Append("<label class='i-cart cssClassCartLabel cssClassGreenBtn'><button type=\"button\" class=\"addtoCart\"");
                    CatWiseItemContains.Append("addtocart=\"");
                    CatWiseItemContains.Append("addtocart");
                    CatWiseItemContains.Append(item.ItemID);
                    CatWiseItemContains.Append("\" title=\"");
                    CatWiseItemContains.Append(itemName);
                    CatWiseItemContains.Append("\" onclick=\"AspxCommerce.RootFunction.AddToCartFromJS(");
                    CatWiseItemContains.Append(item.ItemID);
                    CatWiseItemContains.Append(",");
                    CatWiseItemContains.Append(itemPriceValue);
                    CatWiseItemContains.Append(",'");
                    CatWiseItemContains.Append(itemSKU);
                    CatWiseItemContains.Append("',");
                    CatWiseItemContains.Append(1);
                    CatWiseItemContains.Append(",'");
                    CatWiseItemContains.Append(item.IsCostVariantItem);
                    CatWiseItemContains.Append("',this);\">");
                    CatWiseItemContains.Append(getLocale("Cart +"));
                    CatWiseItemContains.Append("</button></label></div></div>");

                }
            }
            if (GetCustomerID > 0 && GetUsername.ToLower() != "anonymoususer")
            {
                CatWiseItemContains.Append("<div class=\"cssClassWishListButton\">");
                CatWiseItemContains.Append("<label class='i-wishlist cssWishListLabel cssClassDarkBtn'><button type=\"button\" id=\"addWishList\" onclick=AspxCommerce.RootFunction.CheckWishListUniqueness(");
                CatWiseItemContains.Append(item.ItemID);
                CatWiseItemContains.Append(",'");
                CatWiseItemContains.Append(item.SKU);
                CatWiseItemContains.Append("',this);><span>");
                CatWiseItemContains.Append(getLocale("Wishlist+"));
                CatWiseItemContains.Append("</span></button></label></div>");
            }
            else
            {
                CatWiseItemContains.Append("<div class=\"cssClassWishListButton\">");
                CatWiseItemContains.Append("<label class='i-wishlist cssWishListLabel cssClassDarkBtn'><button type=\"button\" id=\"addWishList\" onclick=\"AspxCommerce.RootFunction.Login();\">");
                CatWiseItemContains.Append("<span>");
                CatWiseItemContains.Append(getLocale("Wishlist+"));
                CatWiseItemContains.Append("</span></button></label></div>");
            }
            CatWiseItemContains.Append("</div>");
            CatWiseItemContains.Append("</div></div>");
        }
        else
        {
            string viewMore = "<a href=\"" + aspxRedirectPath + "category/" + AspxUtility.fixedEncodeURIComponent(item.CategoryName) +
                              pageExtension + "\">" + getLocale("View More") + "</a>";
            CatWiseItemContains.Append("<div id=\"divViewMore_" + item.CategoryID + "\" class=\"cssViewMore\">");
            CatWiseItemContains.Append(viewMore);
            CatWiseItemContains.Append("</div>");
        }

    }


    private bool IsExistedCategory(List<int> arr, int cat)
    {
        bool isExist = false;
        int i;
        for (i = 0; i < arr.Count; i++)
        {
            if (arr[i] == cat)
            {
                isExist = true;
                break;
            }
        }
        return isExist;
    }

    private string getLocale(string messageKey)
    {
        string retStr = messageKey;
        if (hst != null && hst[messageKey] != null)
        {
            retStr = hst[messageKey].ToString();
        }
        return retStr;
    }
}
